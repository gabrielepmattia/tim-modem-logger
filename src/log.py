from datetime import datetime


class Log:
    @staticmethod
    def log(module, string):
        time = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        print(f"{time} [{module}] {string}")
