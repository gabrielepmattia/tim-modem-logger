from influxdb import InfluxDBClient
from datetime import datetime

from models import ADSLMetrics


class Influx:
    def __init__(self, host='localhost', port=8086, user='root', pwd='root', db='example'):
        self._client = InfluxDBClient(host, port, user, pwd, db)
        self._client.create_database(db)

    def prepare_point_dict(self, time: str, name: str, value: float, router_id: str):
        return {
            "measurement": name,
            "time": time,
            "tags": {
                "router_id": router_id
            },
            "fields": {
                "value": value
            }
        }

    def logADSLMetrics(self, router_sn: str, metrics: ADSLMetrics):
        if router_sn is None or metrics is None:
            return
        
        time = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        json_body = []
        json_body.append(self.prepare_point_dict(time, "bitrate_up", metrics.bitrate_up, router_sn))
        json_body.append(self.prepare_point_dict(time, "bitrate_down", metrics.bitrate_down, router_sn))
        json_body.append(self.prepare_point_dict(time, "bitrate_max_up", metrics.bitrate_max_up, router_sn))
        json_body.append(self.prepare_point_dict(time, "bitrate_max_down", metrics.bitrate_max_down, router_sn))
        json_body.append(self.prepare_point_dict(time, "noise_up", metrics.noise_up, router_sn))
        json_body.append(self.prepare_point_dict(time, "noise_down", metrics.noise_down, router_sn))
        json_body.append(self.prepare_point_dict(time, "power_up", metrics.power_up, router_sn))
        json_body.append(self.prepare_point_dict(time, "power_down", metrics.power_down, router_sn))
        json_body.append(self.prepare_point_dict(time, "delay_up", metrics.delay_up, router_sn))
        json_body.append(self.prepare_point_dict(time, "delay_down", metrics.delay_down, router_sn))
        json_body.append(self.prepare_point_dict(time, "noise_prot_up", metrics.noise_prot_up, router_sn))
        json_body.append(self.prepare_point_dict(time, "noise_prot_down", metrics.noise_prot_down, router_sn))
        json_body.append(self.prepare_point_dict(time, "attenuation_up", metrics.attenuation_up, router_sn))
        json_body.append(self.prepare_point_dict(time, "attenuation_down", metrics.attenuation_down, router_sn))
        json_body.append(self.prepare_point_dict(time, "pm_es_15", metrics.pm_es_15, router_sn))
        json_body.append(self.prepare_point_dict(time, "pm_ses_15", metrics.pm_ses_15, router_sn))
        json_body.append(self.prepare_point_dict(time, "pm_fec_15", metrics.pm_fec_15, router_sn))
        json_body.append(self.prepare_point_dict(time, "pm_lpr_15", metrics.pm_lpr_15, router_sn))
        json_body.append(self.prepare_point_dict(time, "pm_uas_15", metrics.pm_uas_15, router_sn))
        json_body.append(self.prepare_point_dict(time, "pm_reinits_failed_15", metrics.pm_reinits_failed_15, router_sn))
        self._client.write_points(json_body)
