class ADSLMetrics():
    def __init__(self):
        self.bitrate_up = 0.0
        self.bitrate_down = 0.0
        self.bitrate_max_up = 0.0
        self.bitrate_max_down = 0.0
        self.noise_up = 0.0
        self.noise_down = 0.0
        self.power_up = 0.0
        self.power_down = 0.0
        self.delay_up = 0.0
        self.delay_down = 0.0
        self.noise_prot_up = 0.0
        self.noise_prot_down = 0.0
        self.attenuation_up = 0.0
        self.attenuation_down = 0.0
        self.pm_es_15 = 0
        self.pm_ses_15 = 0
        self.pm_fec_15 = 0
        self.pm_cv_15 = 0
        self.pm_lpr_15 = 0
        self.pm_uas_15 = 0
        # self.pm_reinits_15 = 0
        self.pm_reinits_failed_15 = 0
