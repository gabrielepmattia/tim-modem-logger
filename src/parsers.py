from bs4 import BeautifulSoup

from models import ADSLMetrics


class Parsers:

    @staticmethod
    def parse_expert_data(html) -> ADSLMetrics or None:
        soup = BeautifulSoup(html, 'html.parser')
        data_blocks = soup.select("dl", class_=".d2-horizontal.no-margin.status-list")
        if len(data_blocks) == 0:
            return None

        data_text = data_blocks[0].text
        data_text_lines = data_text.split("\n")
        # for i, line in enumerate(data_text_lines):
        #     print(f"{i} | {line}")

        extract_tuple = lambda line_str: (
            float(line_str.split("/")[0].strip().split(" ")[0]), float(line_str.split("/")[1].strip().split(" ")[0]))

        metrics = ADSLMetrics()

        tmp = extract_tuple(data_text_lines[7])
        metrics.bitrate_up = tmp[0]
        metrics.bitrate_down = tmp[1]

        tmp = extract_tuple(data_text_lines[11])
        metrics.bitrate_max_up = tmp[0]
        metrics.bitrate_max_down = tmp[1]

        tmp = extract_tuple(data_text_lines[13])
        metrics.noise_up = tmp[0]
        metrics.noise_down = tmp[1]

        tmp = extract_tuple(data_text_lines[15])
        metrics.power_up = tmp[0]
        metrics.power_down = tmp[1]

        tmp = extract_tuple(data_text_lines[17])
        metrics.delay_up = tmp[0]
        metrics.delay_down = tmp[1]

        tmp = extract_tuple(data_text_lines[19])
        metrics.noise_prot_up = tmp[0]
        metrics.noise_prot_down = tmp[1]

        tmp = extract_tuple(data_text_lines[21])
        metrics.attenuation_up = tmp[0]
        metrics.attenuation_down = tmp[1]

        metrics.pm_es_15 = int(data_text_lines[24])
        metrics.pm_ses_15 = int(data_text_lines[26])
        metrics.pm_fec_15 = int(data_text_lines[28])
        metrics.pm_cv_15 = int(data_text_lines[30])
        metrics.pm_lpr_15 = int(data_text_lines[32])
        metrics.pm_uas_15 = int(data_text_lines[24])
        # metrics.pm_reinits_15 = int(data_text_lines[24])
        metrics.pm_reinits_failed_15 = int(data_text_lines[38])

        return metrics

    @staticmethod
    def parse_modem_sn(html) -> str:
        soup = BeautifulSoup(html, 'html.parser')

        page_text_lines = soup.text.split("\n")
        # for i, line in enumerate(page_text_lines):
        #     print(f"{i} | {line}")

        return page_text_lines[17].strip()
