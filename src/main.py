import requests
import time
import hashlib
from urllib.parse import quote
from envparse import env

from influx import Influx
from log import Log
from models import ADSLMetrics
from parsers import Parsers

ROUTER_HOST = "192.168.1.1" if env.str("ROUTER_HOST", default="") == "" else env.str("ROUTER_HOST", default="")
ADMIN_USER = "Administrator" if env.str("ADMIN_USER", default="") == "" else env.str("ADMIN_USER", default="")
ADMIN_PWD = "admin" if env.str("ADMIN_PWD", default="") == "" else env.str("ADMIN_PWD", default="")
REFRESH_SECONDS = 180 if env.str("REFRESH_SECONDS", default="") == "" else int(env.str("REFRESH_SECONDS", default=""))

INFLUX_HOST = "localhost" if env.str("INFLUX_HOST", default="") == "" else env.str("INFLUX_HOST", default="")
INFLUX_PORT = 8086 if env.str("INFLUX_PORT", default="") == "" else int(env.str("INFLUX_PORT", default=""))
INFLUX_USER = "root" if env.str("INFLUX_USER", default="") == "" else int(env.str("INFLUX_USER", default=""))
INFLUX_PWD = "root" if env.str("INFLUX_PWD", default="") == "" else int(env.str("INFLUX_PWD", default=""))
INFLUX_DB = "modem-tim" if env.str("INFLUX_DB", default="") == "" else int(env.str("INFLUX_DB", default=""))

MODULE = "MAIN"


def retrieve_data(user, pwd) -> (str or None, ADSLMetrics or None):
    try:
        # API#1: retrieve session id
        res_1 = requests.get("http://192.168.1.1/login.lp")
        cookie = res_1.headers["set-cookie"]

        # API#2: retrieve pre-auth nonce
        res_2 = requests.get("http://192.168.1.1/login.lp?get_preauth=true", headers={"cookie": cookie})
        res_2_text = res_2.text
        res_2_parts = res_2_text.strip().split("|")

        if len(res_2_parts) < 4:
            Log.log(MODULE, "Malformed response, not getting any data, skipping")
            return None, None

        # prepare auth payload
        rn = res_2_parts[0]
        realm = res_2_parts[1]
        nonce = res_2_parts[2]
        qop = res_2_parts[3]
        uri = "/login.lp"

        ha_1 = hashlib.md5(f"{user}:{realm}:{pwd}".encode('utf-8')).hexdigest()
        ha_2 = hashlib.md5(f"GET:{uri}".encode('utf-8')).hexdigest()
        hidepw = hashlib.md5(
            f"{ha_1}:{nonce}:00000001:xyz:{qop}:{ha_2}".encode('utf-8')).hexdigest()

        # API#3: authenticate
        res_3 = requests.post("http://192.168.1.1/login.lp", data=f"rn={quote(rn)}&hidepw={hidepw}&user={user}",
                              headers={
                                  "content-type": "application/x-www-form-urlencoded",
                                  "cookie": cookie
                              })

        # API#4: get data dsl data
        res_4 = requests.get(
            "http://192.168.1.1/network-expert-dsl.lp?ip=&phoneType=undefined", headers={"cookie": cookie})
        metrics = Parsers.parse_expert_data(res_4.text)

        # API#5: get router sn
        res_5 = requests.get("http://192.168.1.1/tim-info.lp?ip=&phoneType=undefined", headers={"cookie": cookie})
        router_sn = Parsers.parse_modem_sn(res_5.text)
        return router_sn, metrics
    except Exception as e:
        Log.log(MODULE, f"Error while retrieving data: {e}")
        return None


def main():
    Log.log(MODULE, "Starting connection towards influxDB")
    client = Influx(host=INFLUX_HOST, port=INFLUX_PORT, user=INFLUX_USER, pwd=INFLUX_PWD, db=INFLUX_DB)

    while True:
        Log.log(MODULE, "Retrieving data")
        sn, metrics = retrieve_data("Administrator", "admin")
        Log.log(MODULE, "Logging data")
        client.logADSLMetrics(sn, metrics)
        Log.log(MODULE, "Sleeping...")
        time.sleep(float(REFRESH_SECONDS))


main()
