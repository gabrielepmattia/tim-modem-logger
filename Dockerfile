FROM python:3.7.9

WORKDIR /app

COPY src .

RUN pip install -r requirements.txt

CMD ["python", "main.py"]