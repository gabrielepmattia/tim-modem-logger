# Technicolor AGCOMBO Logger (Modem TIM)

This application logs ADSL expert data from the AGCOMBO modem to an influxDB server, that you can then plot with Grafana. I used it for monitoring the quality of my ADSL connection over time.

## Grafana Dashboard

In the grafana folder you can find the model for the following dashboard

<a href="https://imgur.com/7iO77Yt"><img src="https://i.imgur.com/7iO77Yt.png" title="source: imgur.com" /></a>